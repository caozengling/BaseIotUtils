package com.wave_chtj.example.callback;

import com.wave_chtj.example.entity.NetBean;

public interface INetTimerCallback {
    void refreshNet(NetBean netBean);
}
